import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Pressable, Modal, FlatList, TouchableOpacity, Text, View, TextInput, Alert, ScrollView, ImageBackground} from 'react-native';

export default function App() {
  
  const sampleGoals = [
    {
      id: 1,
      name: "Café"
    },
    {
      id: 2,
      name: "Caca"
    },
    {
      id: 3,
      name: "Clope"
    }
  ];

  const image = { uri: "https://img.wallpapersafari.com/tablet/1200/1920/94/20/hT3s9p.png" };

  const [input, setInput] = useState("");

  const [listGoals, setGoals] = useState(sampleGoals);

  const [modalVisible, setModalVisible] = useState(false);

  const renderItem = ({ item }) => (
    <View style={styles.buttonContainer}>
      <Text id={item.id} onChangeText={item => setInput(item)} style={styles.liste}>{'\u2B24'}{item.name}</Text>
      {buttonSupp(item.id)}
    </View>
  );

  const addElementToArray =()=>{
    if(input != ''){
      setGoals([...listGoals,{id:listGoals.length+1,name:input}])
      setModalVisible(false);
    }
  }

  const buttonSupp = (id) => {
    return <TouchableOpacity style={styles.button} 
    title="Supprimer" value="Delete" onPress={() => deleteElementToArray(id)}>
      <Text style={styles.text}>Delete</Text></TouchableOpacity>
  };

  const deleteElementToArray = (id) => {
    //console.log({item});
    const Goals = listGoals.filter((item) => item.id !== id);
    setGoals(Goals);
  };

  return (
    <View style={styles.container}>
    <ImageBackground source={image} style={styles.image}>
      <View style={styles.titre}>
        <Text style={styles.textContainer}>Open up </Text><Text style={styles.textContainerBold}>App.js</Text><Text style={styles.textContainer}> to start working on your app!</Text>
      </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.button} value="Add" onPress={() => setModalVisible(true)}><Text style={styles.text}>Ajouter</Text></TouchableOpacity>
        </View>
        <View style={styles.listeContainer}>
        <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>Ajouter un élément a la liste</Text>
              <TextInput style={styles.input} placeholder="Ajouter un élément a la liste" onChangeText={item => setInput(item) }/>
              <View style={styles.buttons}>
                <TouchableOpacity style={styles.button} value="Annuler" onPress={() => setModalVisible(false)}><Text style={styles.text}>Annuler</Text></TouchableOpacity>
                <TouchableOpacity style={styles.button} value="Ajouter" onPress={addElementToArray}><Text style={styles.text}>Add</Text></TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <ScrollView>
          <View style={styles.liste}>
            <FlatList
              data={listGoals}
              renderItem={renderItem}
              keyExtractor={item => item.id}
            />
          </View>
        </ScrollView>
        </View>
    </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  titre: {
    alignItems: 'center',
    marginBottom: 40
  },
  textContainer: {
    color: 'white',
    fontSize: 20,
  },
  buttons: {
    flexDirection: "row"
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  modalView: {
    margin: 20,
    backgroundColor: "grey",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  text: {
    color: 'white'
  },
  textContainerBold: {
    fontWeight: 'bold',
    color: 'red',
    fontSize: 20
  }, 
  input: {
    borderWidth: 2,
    padding: 2,
    color:"white"
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: 25
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },
  liste: {
    alignItems: 'center',
    color: 'white',
  },
  button: {
    borderWidth: 2,
    borderColor: 'red',
    color: 'red',
    fontSize: 15
  },
  listeContainer: {
  }
});